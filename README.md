# Pogo

The [pogo problem](problems/Pogo.md) is a problem that I discovered while working on an optimizing BrainFuck compiler; it is a simplified model of how loops work in BrainFuck.

This repository is my effort to optimize and expand on the pogo problem to see how close I can get to solving the halting problem.

Descriptions of the problems in plain English can be found in the [problems](problems/) directory.


## Vocabulary

* All of the problems can not have a solution. If this is the case I say that it `diverges`.
